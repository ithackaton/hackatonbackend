package com.droidcon.it.hackaton.data;

public class User {
    private String username;
    private String fcmId;
    private String deviceName;

    public User(String username, String fcmId, String deviceName) {
        this.username = username;
        this.fcmId = fcmId;
        this.deviceName = deviceName;
    }

    public User() {
    }

    public String getFcmId() {
        return fcmId;
    }

    public String getUsername() {
        return username;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
