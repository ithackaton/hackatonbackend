package com.droidcon.it.hackaton.data;

/**
 * Created by romans on 08/04/2017.
 */
public class Event {

    public String deviceId;
    public EventType eventType;
    public double value;

    public enum EventType {
        HUMIDITY,
        FIRE
    }
}
