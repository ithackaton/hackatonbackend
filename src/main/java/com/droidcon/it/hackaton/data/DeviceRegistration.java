package com.droidcon.it.hackaton.data;

/**
 * Created by romans on 09/04/2017.
 */
public class DeviceRegistration {
    String deviceName;
    String fcmId;

    public String getDeviceName() {
        return deviceName;
    }

    public String getFcmId() {
        return fcmId;
    }
}
