package com.droidcon.it.hackaton.data;

/**
 * Created by romans on 09/04/2017.
 */
public class NewDeviceResponse {
    public String uuid;

    public NewDeviceResponse(String uuid) {
        this.uuid = uuid;
    }

    public NewDeviceResponse() {
    }
}
