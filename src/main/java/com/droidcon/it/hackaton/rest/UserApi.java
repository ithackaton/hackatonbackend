package com.droidcon.it.hackaton.rest;

import com.droidcon.it.hackaton.data.Event;
import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.fcm.FCMSender;
import com.droidcon.it.hackaton.persistence.CustomerRepository;
import com.droidcon.it.hackaton.persistence.dao.EventData;
import com.droidcon.it.hackaton.services.DeviceService;
import com.droidcon.it.hackaton.services.UserService;
import com.google.android.gcm.server.Message;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Named;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserApi {

    @Autowired
    FCMSender fcmSender;
    @Autowired
    UserService userService;
    @Autowired
    DeviceService deviceService;
    @Named("eventToData")
    @Autowired
    Function<Event, EventData> transformation;

    public static final Logger LOGGER = Logger.getLogger(UserApi.class.getSimpleName());

    @RequestMapping(method = RequestMethod.POST, path = "/register",consumes = "application/json")
    public ResponseEntity register(@RequestBody User user) {
        userService.registerUser(user.getUsername(), user.getFcmId(), user.getDeviceName());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity getAllUsersForDevice(@RequestBody String deviceUUID) {
        List<User>deviceUsers = deviceService.getDeviceUsers(deviceUUID);
        return ResponseEntity.ok(new Gson().toJson(deviceUsers));
    }
}
