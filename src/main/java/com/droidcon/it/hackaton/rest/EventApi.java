package com.droidcon.it.hackaton.rest;
import com.droidcon.it.hackaton.data.Event;
import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.fcm.FCMSender;
import com.droidcon.it.hackaton.persistence.CustomerRepository;
import com.droidcon.it.hackaton.persistence.dao.EventData;
import com.droidcon.it.hackaton.services.DeviceService;
import com.google.android.gcm.server.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Named;

@RestController
@RequestMapping("/events")
public class EventApi {

    @Autowired
    FCMSender fcmSender;
    @Autowired
    CustomerRepository repository;
    @Autowired
    DeviceService deviceService;
    @Named("eventToData")
    @Autowired
    Function<Event, EventData> transformation;

    public static final Logger LOGGER = Logger.getLogger(EventApi.class.getSimpleName());

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity event(@RequestBody Event event) {
        repository.save(transformation.apply(event));
        List<User> deviceUsers = deviceService.getDeviceUsers(event.deviceId);
        fcmSender
            .sendMessage(new Message.Builder().addData("alert", event.eventType.name()).build(),
        deviceUsers.stream().map(User::getFcmId).collect(Collectors.toList()));
        return ResponseEntity.ok().build();
    }
}
