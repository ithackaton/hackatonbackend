package com.droidcon.it.hackaton.rest;

import com.droidcon.it.hackaton.data.DeviceRegistration;
import com.droidcon.it.hackaton.data.NewDeviceResponse;
import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.exceptions.DeviceIdNotUniqueException;
import com.droidcon.it.hackaton.persistence.dao.DeviceData;
import com.droidcon.it.hackaton.services.DeviceService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.POST;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by romans on 09/04/2017.
 */
@RestController
@RequestMapping("/devices")
public class DeviceApi {
    @Autowired
    DeviceService service;

    @RequestMapping(method = RequestMethod.POST, path = "/create", consumes = "application/json")
    public ResponseEntity<NewDeviceResponse> registerDevice(@RequestBody DeviceRegistration registration) {
        try {
            Logger.getAnonymousLogger().log(Level.SEVERE, new Gson().toJson(registration));
            String newDevice = service.createNewDevice(registration.getDeviceName(), registration.getFcmId());
            return ResponseEntity.ok(new NewDeviceResponse(newDevice));
        } catch (DeviceIdNotUniqueException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Error occured", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }


    @RequestMapping(method = RequestMethod.GET, path = "/devices", consumes = "application/json")
    public ResponseEntity getAllDevices() {
        List<DeviceData> deviceUsers = service.allDevices();
        return ResponseEntity.ok(new Gson().toJson(deviceUsers));
    }
}
