package com.droidcon.it.hackaton.persistence;

import com.droidcon.it.hackaton.persistence.dao.DeviceData;
import com.droidcon.it.hackaton.persistence.dao.UserData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by romans on 09/04/2017.
 */
public interface DeviceRepository extends CrudRepository<DeviceData, Long> {

    Long countByDeviceName(String deviceName);

    List<DeviceData> findByUniquePublicId(String deviceUUID);

    List<DeviceData> findByDeviceName(String deviceName);
}
