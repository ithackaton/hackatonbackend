package com.droidcon.it.hackaton.persistence.functions;

import com.droidcon.it.hackaton.persistence.dao.DeviceData;
import com.droidcon.it.hackaton.persistence.dao.UserData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by romans on 09/04/2017.
 */
public interface UserRepository extends CrudRepository<UserData, Long> {

}
