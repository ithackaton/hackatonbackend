package com.droidcon.it.hackaton.persistence.dao;

import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name = "device")
public class DeviceData {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    long deviceId;
    String deviceName;
    String fcmId;
    String uniquePublicId;
    @OneToMany(fetch =FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    Set<UserData> users;

    public DeviceData(String deviceName, String fcmId, String uniquePublicId) {
        this.deviceName = deviceName;
        this.fcmId = fcmId;
        this.uniquePublicId = uniquePublicId;
    }

    public DeviceData() {
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getFcmId() {
        return fcmId;
    }

    public String getUniquePublicId() {
        return uniquePublicId;
    }

    public Set<UserData> getUsers() {
        return users;
    }
}
