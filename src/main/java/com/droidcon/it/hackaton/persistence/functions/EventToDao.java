package com.droidcon.it.hackaton.persistence.functions;

import com.droidcon.it.hackaton.data.Event;
import com.droidcon.it.hackaton.persistence.dao.EventData;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.function.Function;

/**
 * Created by romans on 08/04/2017.
 */
@Singleton
@Named("eventToData")
public class EventToDao implements Function<Event, EventData> {
    @Override
    public EventData apply(Event event) {
        return new EventData(event.deviceId, event.eventType, event.value);
    }
}
