package com.droidcon.it.hackaton.persistence.functions;

import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.persistence.dao.UserData;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.function.Function;

/**
 * Created by romans on 09/04/2017.
 */
@Singleton
@Named(Transformation.USER_TO_DTO)
public class UserToDto implements Function<UserData, User> {
    @Override
    public User apply(UserData userData) {
        return new User(userData.getUserEmail(), userData.getFcmId(), userData.getDevice().getDeviceName());
    }
}
