package com.droidcon.it.hackaton.persistence.dao;


import com.droidcon.it.hackaton.data.Event;

import javax.persistence.*;

/**
 * Created by romans on 08/04/2017.
 */
@Entity(name = "event")
public class EventData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    long id;
    public String deviceId;
    @Enumerated
    public Event.EventType eventType;
    public double value;

    public EventData(String deviceId, Event.EventType eventType, double value) {
        this.deviceId = deviceId;
        this.eventType = eventType;
        this.value = value;
    }

    public EventData() {
    }

    @Override
    public String toString() {
        return "EventData{" +
                "id=" + id +
                ", deviceName='" + deviceId + '\'' +
                ", eventType=" + eventType +
                ", value=" + value +
                '}';
    }
}
