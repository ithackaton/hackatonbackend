package com.droidcon.it.hackaton.persistence.dao;

import javax.persistence.*;

/**
 * Created by romans on 09/04/2017.
 */
@Entity(name = "user")
public class UserData {

    private String name;
    private String password;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    String userEmail;
    String userPassword;
    String fcmId;
    @ManyToOne(cascade={ CascadeType.ALL })
    @JoinColumn(name = "deviceId")
    DeviceData device;

    public UserData(String name, String password, String fcmId, DeviceData deviceData) {
        this.name = name;
        this.password = password;
        this.fcmId = fcmId;
        this.device = deviceData;
    }

    public UserData(String name, String password, String fcmId) {
        this.name = name;
        this.password = password;
        this.fcmId = fcmId;
    }

    public UserData() {
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getFcmId() {
        return fcmId;
    }

    public DeviceData getDevice() {
        return device;
    }
}
