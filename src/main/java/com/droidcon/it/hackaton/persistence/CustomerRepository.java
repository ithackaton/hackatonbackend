package com.droidcon.it.hackaton.persistence;

import com.droidcon.it.hackaton.data.Event;
import com.droidcon.it.hackaton.persistence.dao.EventData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<EventData, Long> {

    List<EventData> findByDeviceId(String deviceId);
}