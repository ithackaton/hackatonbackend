package com.droidcon.it.hackaton.services;

import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.exceptions.DeviceIdNotUniqueException;
import com.droidcon.it.hackaton.persistence.DeviceRepository;
import com.droidcon.it.hackaton.persistence.dao.DeviceData;
import com.droidcon.it.hackaton.persistence.dao.UserData;
import com.droidcon.it.hackaton.persistence.functions.Transformation;
import com.droidcon.it.hackaton.persistence.functions.UserRepository;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import javax.inject.Named;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by romans on 09/04/2017.
 */
@Service
public class UserService {

    @Autowired
    @Named(Transformation.USER_TO_DTO)
    Function<UserData, User> userToDto;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DeviceRepository deviceService;

    @Transactional
    public void registerUser(String name, String fcmId, String deviceName) {
        deviceService.findByDeviceName(deviceName).get(0).getUsers().add(new UserData(name, ""/*TODO*/, fcmId));
        Logger.getAnonymousLogger().log(Level.SEVERE, new Gson().toJson(deviceService.findByDeviceName(deviceName).get(0).getUsers()));
    }
}
