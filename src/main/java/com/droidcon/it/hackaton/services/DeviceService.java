package com.droidcon.it.hackaton.services;

import com.droidcon.it.hackaton.data.User;
import com.droidcon.it.hackaton.exceptions.DeviceIdNotUniqueException;
import com.droidcon.it.hackaton.persistence.DeviceRepository;
import com.droidcon.it.hackaton.persistence.dao.DeviceData;
import com.droidcon.it.hackaton.persistence.dao.UserData;
import com.droidcon.it.hackaton.persistence.functions.Transformation;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by romans on 09/04/2017.
 */
@Service
public class DeviceService {

    @Autowired
    DeviceRepository repository;
    @Autowired
    @Named(Transformation.USER_TO_DTO)
    Function<UserData, User> userToDto;

    @Transactional
    public String createNewDevice(String name, String fcmId) throws DeviceIdNotUniqueException {
        Long hasThisEntry = repository.countByDeviceName(name);
        if (hasThisEntry != 0) {
            throw new DeviceIdNotUniqueException();
        }
        DeviceData deviceData = new DeviceData(name, fcmId, UUID.randomUUID().toString());
        DeviceData data = repository.save(deviceData);
        return repository.findByDeviceName(name).get(0).getUniquePublicId();
    }

    @Transactional
    public List<User> getDeviceUsers(String deviceUUID) {
        List<DeviceData> byUniquePublicId = repository.findByUniquePublicId(deviceUUID);
        Preconditions.checkArgument(byUniquePublicId.size() == 1, String.format("Device is not unique or does not exist: %s", byUniquePublicId.size()));
        return byUniquePublicId.get(0).getUsers().stream().map(userToDto).collect(Collectors.toList());
    }

    @Transactional
    public List<DeviceData> allDevices() {
        return Lists.newArrayList(repository.findAll());
    }
}
