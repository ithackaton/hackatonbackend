package com.droidcon.it.hackaton.fcm;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@Async("notificationExecutor")
public class FCMSender extends Sender {

    public static final Logger LOGGER = Logger.getLogger(FCMSender.class.getSimpleName());

    public FCMSender(@Value("${fcm.api.key}") String key) {
        super(key);
    }

    @Override
    protected HttpURLConnection getConnection(String url) throws IOException {
        String fcmUrl = "https://fcm.googleapis.com/fcm/send";
        return (HttpURLConnection) new URL(fcmUrl).openConnection();
    }

    public void sendMessage(Message alert, List<String> userIds) {
        try {
            if (userIds == null) {
                userIds = Lists.newArrayList();
            }
            //Dirty hack for not registered user
            String userId = "cBYopuzXEks:APA91bE1Q6a6MaOqPSHKo-aBfOlzlkmHUr2KkvomJDWiSIAMTM9wpADEmIImtaiX5erMdmSgVtp_uMR8I3eFKTMaOTk8M2nL9F_-nDxXWG24mpMOhsGaeygSAKS_WRLHFx1Znq-zRWf9";
            userIds.add(userId);
            send(alert, userIds, 5);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, String.format("Error occured while sending push to user: %s", userIds), e);
        }
    }
}